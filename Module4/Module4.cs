﻿using System;

namespace M4
{
    public class Module4
    {
        static void Main(string[] args)
        {       
            // Метод является точкой входа.
            // Остается пустым, т.к. все методы класса тестируются автоматически!
        }

        public int Task_1_A(int[] array)
        {
            if (array == null || array.Length == 0)
                throw new ArgumentNullException("array", "Is Empty");

            int max = array[0];
            if (array.Length > 1)
            {
                for (int i = 1; i < array.Length; i++)
                {
                    if (max < array[i])
                    {
                        max = array[i];
                    }
                }
            }
            return max;
        }

        public int Task_1_B(int[] array)
        {
            if (array == null || array.Length == 0)
                throw new ArgumentNullException("array", "Is Empty");

            int min = array[0];
            if (array.Length > 1)
            {
                for (int i = 1; i < array.Length; i++)
                {
                    if (min > array[i])
                    {
                        min = array[i];
                    }
                }
            }
            return min;
        }

        public int Task_1_C(int[] array)
        {
            if (array == null || array.Length == 0)
                throw new ArgumentNullException("array", "Is Empty");

            int sum = array[0];
            if (array.Length > 1)
            {
                for (int i = 1; i < array.Length; i++)
                {
                    sum += array[i];
                }
            }
            return sum;
        }

        public int Task_1_D(int[] array)
        {
            if (array == null || array.Length == 0)
                throw new ArgumentNullException("array", "Is Empty");

            if (array.Length == 1)
                return 0;

            Module4 module = new Module4();
            int max = module.Task_1_A(array);
            int min = module.Task_1_B(array);
            return max - min;
        }

        public void Task_1_E(int[] array)
        {
            if (array == null || array.Length == 0)
                throw new ArgumentNullException("array", "Is Empty");

            Module4 module = new Module4();
            int max = module.Task_1_A(array);
            int min = module.Task_1_B(array);

            for (int i = 0; i < array.Length; i++)
            {
                if (i % 2 == 0)
                {
                    array[i] += max;
                }
                else
                {
                    array[i] -= min;
                }
            }
        }

        public int Task_2(int a, int b, int c)
        {
            return a + b + c;
        }

        public int Task_2(int a, int b)
        {
            return a + b;
        }

        public double Task_2(double a, double b, double c)
        {
            return a + b + c;
        }

        public string Task_2(string a, string b)
        {
            return new string(a + b);
        }

        public int[] Task_2(int[] a, int[] b)
        {
            if (a.Length > b.Length)
            {
                for (int i = 0; i < b.Length; i++)
                {
                    a[i] += b[i];
                }
                return a;
            }
            else
            {
                for (int i = 0; i < a.Length; i++)
                {
                    b[i] += a[i];
                }
                return b;
            }
        }

        public void Task_3_A(ref int a, ref int b, ref int c)
        {
            a += 10;
            b += 10;
            c += 10;
        }

        public void Task_3_B(double radius, out double length, out double square)
        {
            if (radius < 0)
            {
                throw new ArgumentException("Is negative", "radius");
            }
            length = 2 * Math.PI * radius;
            square = Math.PI * Math.Pow(radius, 2);
        }

        public void Task_3_C(int[] array, out int maxItem, out int minItem, out int sumOfItems)
        {
            if (array == null || array.Length == 0)
                throw new ArgumentNullException("array", "Is Empty");

            Module4 module = new Module4();
            maxItem = module.Task_1_A(array);
            minItem = module.Task_1_B(array);
            sumOfItems = module.Task_1_C(array);
        }

        public (int, int, int) Task_4_A((int, int, int) numbers)
        {
            numbers.Item1 += 10;
            numbers.Item2 += 10;
            numbers.Item3 += 10;
            return numbers;
        }

        public (double, double) Task_4_B(double radius)
        {
            if (radius < 0)
                throw new ArgumentException("Is negative", "radius");

            return (2 * Math.PI * radius, Math.PI * Math.Pow(radius, 2));
        }

        public (int, int, int) Task_4_C(int[] array)
        {
            if (array == null || array.Length == 0)
                throw new ArgumentNullException("array", "Is Empty");

            Module4 module = new Module4();

            return (module.Task_1_B(array), module.Task_1_A(array), module.Task_1_C(array));

        }

        public void Task_5(int[] array)
        {
            if (array == null || array.Length == 0)
                throw new ArgumentNullException("array", "Is Empty");

            for (int i = 0; i < array.Length; i++)
            {
                array[i] += 5;
            }
        }

        public void Task_6(int[] array, SortDirection direction)
        {
            if (array == null || array.Length == 0)
                throw new ArgumentNullException("array", "Is Empty");

            Array.Sort(array);
            if (direction == SortDirection.Descending)
                Array.Reverse(array);
        }

        public double Task_7(Func<double, double> func, double x1, double x2, double e, double result = 0)
        {
            if (func == null)
                throw new ArgumentNullException("func", "Is Empty");

            result = (x1 + x2) / 2;
            if (x2 - x1 > e)
            {
                double x = (x1 + x2) / 2;

                if (func(x2) * func(x) < 0)
                    x1 = x;
                else
                    x2 = x;

                result = Task_7(func, x1, x2, e, result);
            }
            return result;            
        }
    }
}